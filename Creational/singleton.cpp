#include "singleton.h"

Singleton::Singleton(const std::string &value) : mvalue(value)
{}

Singleton *Singleton::getInstance(const std::string &value)
{
    if (mSingletonInstance == nullptr) {
        mSingletonInstance = new Singleton(value);
    }

    return mSingletonInstance;
}

std::string Singleton::value() const
{
    return mvalue;
}

std::string Singleton::value(const std::string &value)
{
    mvalue = value;
}

Singleton*  Singleton::mSingletonInstance = nullptr;

