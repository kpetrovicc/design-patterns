#ifndef SINGLETON_H
#define SINGLETON_H

#include <string>

class Singleton
{
private:
    /* Default empty costructor. */
    Singleton() = default;

    /* Constructor. */
    Singleton(const std::string &value);

    /* Static member with value of unique instance. */
    static Singleton* mSingletonInstance;

    /* Member value. */
    std::string mvalue;
public:
    /* In Singleton we can't copy or assign instance. */
    Singleton(Singleton& other) = delete;
    void operator=(const Singleton &other) = delete;

    /* Return existing instance or create new if instance doesn't exists. */
    static Singleton *getInstance(const std::string &value);

    /* Return value of member. */
    std::string value() const;
    std::string value(const std::string &value);


};

#endif // SINGLETON_H
