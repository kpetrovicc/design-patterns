#include <iostream>
#include "singleton.h"

using namespace std;

int main()
{

    Singleton *first = Singleton::getInstance("first");
    Singleton *second = Singleton::getInstance("second");


    std::cout << first->value() << std::endl;
    std::cout << second->value() << std::endl;

    second->value("changed");

    std::cout << first->value() << std::endl;
    std::cout << second->value() << std::endl;

    /*

      Excepted output:
      first
      first
      changed
      changed

    */

    return 0;
}
